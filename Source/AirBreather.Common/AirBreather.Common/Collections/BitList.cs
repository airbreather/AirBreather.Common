﻿using System;
using System.Collections;
using System.Collections.Generic;

using AirBreather.Common.Utilities;

namespace AirBreather.Common.Collections
{
    public sealed class BitList : IList<bool>, IReadOnlyList<bool>
    {
        private static readonly BitArray Empty = new BitArray(0);

        private BitArray values;

        private int version;
        private int count;

        public BitList()
        {
            this.values = Empty;
        }

        public BitList(int capacity)
        {
            this.values = capacity.ValidateNotLessThan(nameof(capacity), 0) == 0
                ? Empty
                : new BitArray(capacity);
        }

        public BitList(BitArray bitArray)
        {
            this.values = bitArray.ValidateNotNull(nameof(bitArray)).Length == 0
                ? Empty
                : new BitArray(bitArray);

            this.count = this.values.Length;
        }

        public BitList(IEnumerable<bool> values)
        {
            bool[] array = values.ValidateNotNull(nameof(values)) as bool[];
            if (array != null)
            {
                this.values = new BitArray(array);
                this.count = array.Length;
                return;
            }

            int trueInitialCapacity = values.GetCountPropertyIfAvailable() ?? 0;
            this.values = trueInitialCapacity == 0
                ? Empty
                : new BitArray(trueInitialCapacity);

            // Don't bother with AddRange -- it does extra stuff we've already done.
            foreach (bool value in values)
            {
                this.AddCore(value);
            }
        }

        public int Count => this.count;

        public bool IsReadOnly => false;

        public int Capacity
        {
            get
            {
                return this.values.Length;
            }

            set
            {
                if (value < this.count)
                {
                    throw new ArgumentOutOfRangeException(nameof(value), value, "Too many elements.");
                }

                if (value == this.values.Length)
                {
                    return;
                }

                if (this.values == Empty)
                {
                    this.values = new BitArray(value);
                }
                else
                {
                    this.values.Length = value;
                }
            }
        }

        public bool this[int index]
        {
            get
            {
                return this.values[index.ValidateInRange(nameof(index), 0, this.count)];
            }

            set
            {
                this.values[index.ValidateInRange(nameof(index), 0, this.count)] = value;
                this.Modified();
            }
        }

        public void Add(bool value)
        {
            this.AddCore(value);
            this.Modified();
        }

        private void AddCore(bool value)
        {
            this.EnsureCapacity(this.count + 1);
            this.values[this.count++] = value;
        }

        public void AddRange(IEnumerable<bool> values) => this.InsertRange(this.count, values);

        ////public ReadOnlyCollection<bool> AsReadOnly() => new ReadOnlyCollection<bool>(this);

        public void Clear()
        {
            // unlike List<T>, we have absolutely no reason to clear the array itself.
            this.count = 0;
            this.Modified();
        }

        public bool Contains(bool value)
        {
            for (int i = 0; i < this.count; i++)
            {
                if (this.values[i] == value)
                {
                    return true;
                }
            }

            return false;
        }

        public void CopyTo(bool[] array, int arrayIndex) => this.AsReadOnlyCollection().CopyTo(array, arrayIndex);

        private void EnsureCapacity(int neededSize)
        {
            if (this.values.Length < neededSize)
            {
                this.Capacity = (int)Math.Min(Int32.MaxValue,
                                              Math.Max(neededSize,
                                                       unchecked((uint)(this.values.Length == 0
                                                                            ? 128
                                                                            : this.values.Length * 2))));
            }
        }

        public void ForEach(Action<bool> action)
        {
            action.ValidateNotNull(nameof(action));

            foreach (bool value in this)
            {
                action(value);
            }
        }

        public int IndexOf(bool item)
        {
            for (int i = 0; i < this.count; i++)
            {
                if (this.values[i] == item)
                {
                    return i;
                }
            }

            return -1;
        }

        public void Insert(int index, bool value)
        {
            this.InsertCore(index.ValidateInRange(nameof(index), 0, this.count + 1), value);
            this.Modified();
        }

        private void InsertCore(int index, bool value)
        {
            this.EnsureCapacity(this.count + 1);
            for (int i = this.count - 1; i >= index; i--)
            {
                this.values[i + 1] = this.values[i];
            }

            this.values[index] = value;
            this.count++;
        }

        public void InsertRange(int index, IEnumerable<bool> values)
        {
            index.ValidateInRange(nameof(index), 0, this.count + 1);
            int? insertCount = values.ValidateNotNull(nameof(values)).GetCountPropertyIfAvailable();

            if (insertCount.HasValue)
            {
                int trueInsertCount = insertCount.Value;

                this.EnsureCapacity(this.count + trueInsertCount);

                // shift the values after the index over by the width of what we're inserting.
                for (int i = index; i < this.count; i++)
                {
                    this.values[i + trueInsertCount] = this.values[i];
                }

                if (this == values)
                {
                    for (int i = 0; i < index; i++)
                    {
                        this.values[i + index] = this.values[i];
                    }

                    for (int i = index; i < this.count; i++)
                    {
                        this.values[i + index + index] = this.values[i + index + trueInsertCount];
                    }

                    return;
                }

                foreach (bool value in values)
                {
                    this.values[index++] = value;
                }

                this.count += trueInsertCount;
            }
            else
            {
                foreach (bool value in values)
                {
                    this.InsertCore(index++, value);
                }
            }

            this.Modified();
        }

        public bool Remove(bool item)
        {
            int index = this.IndexOf(item);
            if (index < 0)
            {
                return false;
            }

            this.RemoveAt(index);
            return true;
        }

        public void RemoveAt(int index)
        {
            index.ValidateInRange(nameof(index), 0, this.count);
            for (int i = index; i < this.count - 1; i++)
            {
                this.values[i] = this.values[i + 1];
            }

            this.count--;
            this.Modified();
        }

        public void RemoveRange(int index, int count)
        {
            index.ValidateNotLessThan(nameof(index), 0);
            count.ValidateNotLessThan(nameof(count), 0);

            if (count == 0)
            {
                return;
            }

            if (this.count - index < count)
            {
                throw new ArgumentException("Not enough values to remove", nameof(count));
            }

            this.count -= count;
            for (int i = index; i < this.count; i++)
            {
                this.values[i] = this.values[i + count];
            }

            this.Modified();
        }

        public void Reverse()
        {
            int i = 0;
            int j = this.count - 1;
            while (i < j)
            {
                bool temp = this.values[i];
                this.values[i++] = this.values[j];
                this.values[j--] = temp;
            }

            this.Modified();
        }

        public BitArray ToBitArray() => new BitArray(this.values) { Length = this.count };

        public void TrimExcess() => this.Capacity = this.count;

        public IEnumerator<bool> GetEnumerator() => new Enumerator(this);
        IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

        private void Modified() => this.version++;

        private sealed class Enumerator : IEnumerator<bool>
        {
            private readonly BitList lst;

            private readonly int version;

            private int currIndex;

            private bool current;

            internal Enumerator(BitList lst)
            {
                this.lst = lst;
                this.version = lst.version;
                this.currIndex = 0;
            }

            public bool Current => this.current;
            object IEnumerator.Current => Boxes.Boolean(this.current);

            public bool MoveNext()
            {
                if (this.version != this.lst.version)
                {
                    throw new InvalidOperationException("Collection was modified during enumeration.");
                }

                if (this.lst.count <= this.currIndex)
                {
                    return false;
                }

                this.current = this.lst[this.currIndex++];
                return true;
            }

            public void Reset()
            {
                if (this.version != this.lst.version)
                {
                    throw new InvalidOperationException("Collection was modified during enumeration.");
                }

                this.currIndex = 0;
            }

            void IDisposable.Dispose()
            {
            }
        }
    }
}
